import '@babel/polyfill'
import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import VueResource from 'vue-resource'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Default from './layouts/Default.vue'
import GuestHeader from './layouts/GuestHeader.vue'

Vue.component('default-layout', Default)
Vue.component('guest-header-layout', GuestHeader)

Vue.config.productionTip = false

Vue.use(VueResource)
Vue.use(Vuetify)

Vue.use(Vuetify, {
  theme: {
    primary: '#6adca1',
    secondary: '#e7e7e7',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
