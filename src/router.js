import Vue from 'vue'
import Router from 'vue-router'
import Index from './views/Index.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'index',
      meta: { layout: 'guest-header' },
      component: Index
    },
    {
      path: '/contact',
      name: 'contact',
      meta: { layout: 'guest-header' },
      component: () => import(/* webpackChunkName: "contact" */ './views/contact.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue')
    },
    {
      path: '/username',
      name: 'profile',
      component: () => import(/* webpackChunkName: "username" */ './views/Profile.vue')
    },
    {
      path: '/message',
      name: 'message',
      component: () => import(/* webpackChunkName: "message" */ './views/Message.vue')
    },
    {
      path: '/setting',
      name: 'setting',
      component: () => import(/* webpackChunkName: "setting" */ './views/Setting.vue')
    },
    {
      path: '/chatroom',
      name: 'chatroom',
      component: () => import(/* webpackChunkName: "chatroom" */ './views/Chatroom.vue')
    },

    {
      path: '/notification',
      name: 'notification',
      component: () => import(/* webpackChunkName: "notification" */ './views/Notification.vue')
    },
    {
      path: '/compose-message',
      name: 'ComposeMessage',
      component: () => import(/* webpackChunkName: "compose-message" */ './views/ComposeMessage.vue')
    },
    {
      path: '/forgot-password',
      name: 'forgot-password',
      meta: { layout: 'guest-header' }
    },
    {
      path: '/about',
      name: 'about',
      meta: { layout: 'guest-header' },
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/about.vue')
    }
  ]
})
